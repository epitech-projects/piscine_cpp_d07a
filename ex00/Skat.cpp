//
// Skat.cpp for Piscine in /home/gravie_j/Documents/projets/piscine/piscine_cpp_d07a/ex00
//
// Made by Jean Gravier
// Login   <gravie_j@epitech.net>
//
// Started on  Tue Jan 14 19:16:18 2014 Jean Gravier
// Last update Tue Jan 14 22:51:05 2014 Jean Gravier
//

#include "Skat.h"
#include <string>
#include <iostream>

Skat::Skat(std::string const& name, int stimPaks)
{
  this->_name = name;
  this->_stimPaks = stimPaks;
}

Skat::Skat()
{
  this->_name = "bob";
  this->_stimPaks = 15;
}

Skat::~Skat()
{

}

int	&Skat::stimPaks()
{
  return (this->_stimPaks);
}

const	std::string &Skat::name()
{
  return (this->_name);
}

void	Skat::shareStimPaks(int number, int &stock)
{
  if (this->_stimPaks - number >= 0)
    {
      this->_stimPaks -= number;
      stock += number;
      std::cout << "Keep the change." << std::endl;
    }
  else
    std::cout << "Don't be greedy" << std::endl;
}

void	Skat::addStimPaks(unsigned int number)
{
  this->_stimPaks += number;
  if (!number)
    std::cout << "Hey boya, did you forget something ?" << std::endl;
}

void	Skat::useStimPaks()
{
  if (this->_stimPaks - 1)
    {
      this->_stimPaks--;
      std::cout << "Time to kick some ass and chew bubble gum." << std::endl;
    }
  else
    std::cout << "Mediiiiiic" << std::endl;
}

void	Skat::status()
{
  std::cout << "Soldier " << this->_name << " reporting " << this->_stimPaks << " stimpaks remaining sir !" << std::endl;
}
