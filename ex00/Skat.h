/*
** Skat.h for Piscine in /home/gravie_j/Documents/projets/piscine/piscine_cpp_d07a/ex00
**
** Made by Jean Gravier
** Login   <gravie_j@epitech.net>
**
** Started on  Tue Jan 14 19:16:12 2014 Jean Gravier
** Last update Tue Jan 14 22:51:14 2014 Jean Gravier
*/

#ifndef SKAT_H_
# define SKAT_H_

#include <string>

class		Skat
{
 public:
  Skat(std::string const& name, int stimPaks);
  Skat();
  ~Skat();

 public:
  int	&stimPaks();
  const std::string& name();

 public:
  void shareStimPaks(int number, int &stock);
  void addStimPaks(unsigned int number);
  void useStimPaks();
 public:
  void status();
 private:
  std::string	_name;
  int		_stimPaks;
};


#endif /* !SKAT_H_ */
